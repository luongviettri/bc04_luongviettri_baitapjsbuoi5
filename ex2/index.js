function tinhTienDien() {
    var hoTen = document.getElementById("txt-ho-ten").value;


    var soKw = document.getElementById("txt-so-kw").value * 1;


    var resultEl = document.getElementById("result");
    var tongTienDien = 0;
    if (soKw <= 50) {
        tongTienDien = 500 * soKw;
    } else if (soKw <= 100) {
        tongTienDien = 500 * 50 + (soKw - 50) * 650;
    } else if (soKw <= 200) {
        tongTienDien = 500 * 50 + 50 * 650 + (soKw - 100) * 850;
    } else if (soKw <= 350) {
        tongTienDien = 500 * 50 + 50 * 650 + 100 * 850 + (soKw - 200) * 1100;
    } else {
        tongTienDien = 500 * 50 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300;
    }

    resultEl.innerHTML = hoTen + ", Tiền điện: " + tongTienDien;

}