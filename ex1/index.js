function TinhDiemKhuVuc(khuVuc) {
    switch (khuVuc) {
        case "x":
            return 0;
        case "a":
            return 2;
        case "b":
            return 1;
        case "c":
            return 0.5;
    }

}
function TinhDiemDoiTuong(doiTuong) {
    switch (doiTuong) {
        case 0:
            return 0;
        case 1:
            return 2.5;
        case 2:
            return 1.5;
        case 3:
            return 1;
    }
}
function HienThiKetQua() {
    var diemChuanEl = document.getElementById("txt-diem-chuan").value * 1;

    var khuVuc = document.getElementById("txt-khu-vuc").value;
    var doiTuong = document.getElementById("txt-doi-tuong").value * 1;
    var diemMon1 = document.getElementById("txt-diem-mon-1").value * 1;
    var diemMon2 = document.getElementById("txt-diem-mon-2").value * 1;
    var diemMon3 = document.getElementById("txt-diem-mon-3").value * 1;
    var resultEl = document.getElementById("result");
    var tongDiem3Mon = diemMon1 + diemMon2 + diemMon3;
    var diemTongKet = 0;
    if (diemChuanEl == 0) {
        alert("Vui lòng nhập điểm chuẩn");
        return;
    }

    if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
        resultEl.innerHTML = "Bị điểm liệt rồi nha fen"
        return;
    }

    diemTongKet = tongDiem3Mon + TinhDiemKhuVuc(khuVuc) + TinhDiemDoiTuong(doiTuong);

    if (diemTongKet >= diemChuanEl) {
        resultEl.innerHTML = "Chúc mừng fen đã đậu";
    } else {
        resultEl.innerHTML = "Xin lỗi fen rớt rồi";
    }
}

